// Created by Lukas Kohutka

import shift_regs_queue_pkg::*;

//  ----------------------------------------------------------------------------
//
//  ENTITY DECLARATION ---------------------------------------------------------
//
// -----------------------------------------------------------------------------

module shift_regs_queue #(
  parameter CAPACITY = shift_regs_queue_pkg::CAPACITY // less or equal to 2^ID_W
)(
  // ---------------------------------------------------
  // Clock & Reset
  // ---------------------------------------------------
  input   logic   CLK,
  input   logic   RST,

  // ---------------------------------------------------
  // Input Control Signals
  // ---------------------------------------------------
  input   opcode  INSTR,         // from bus

  // ---------------------------------------------------
  // Input Items
  // ---------------------------------------------------
  input   s_item  INPUT_ITEM,    // task from bus
  input   logic [ID_W-1:0] KILL_ID,

  // ---------------------------------------------------
  // Output Items
  // ---------------------------------------------------
  output  s_item  BEST_ITEM      // task with the smallest deadline
);

//  ----------------------------------------------------------------------------
//
// ARCHITECTURE DECLARATION  ---------------------------------------------------
//
// -----------------------------------------------------------------------------

logic [CAPACITY:-1] better_neighbor;
logic [CAPACITY-1:-1] killing_signals;

logic [CAPACITY:-1][ID_W-1:0]   items_id;
logic [CAPACITY:-1][DATA_W-1:0] items_data;

// ----------------------------------------------------------------------------
//
// ARCHITECTURE BODY ----------------------------------------------------------
//
// ----------------------------------------------------------------------------

always_comb begin : right_corner_comb
  better_neighbor[-1] = 1'b0;
  killing_signals[-1] = CAN_KILL ? 1'b0 : 1'b1;
  items_id[-1]        = '0;
  items_data[-1]      = '0;
end : right_corner_comb

always_comb begin : left_corner_comb
  items_id[CAPACITY]   = CONST_ITEM_RESET.id;
  items_data[CAPACITY] = CONST_ITEM_RESET.data;
  better_neighbor[CAPACITY] = 1'b1;
end : left_corner_comb

// --------------------------------------------
// TASK CELLS INSTANTIATION
// --------------------------------------------
generate
  genvar i;
  for (i = 0; i < CAPACITY; i++) begin : task_cells
    sorting_cell #(
      .i(i)
    ) sorting_cell_inst (
      // ---------------------------------------------------
      // Clock & Reset
      // ---------------------------------------------------
      .CLK              (CLK),
      .RST              (RST),

      // ---------------------------------------------------
      // Input Control Signals
      // ---------------------------------------------------
      .INSTR            (INSTR),
      .SHIFT            (|killing_signals[i-1:-1]),
      .L_BETTER         (better_neighbor[i+1]),
      .R_BETTER         (better_neighbor[i-1]),

      // ---------------------------------------------------
      // Output Control Signals
      // ---------------------------------------------------
      .BETTER           (better_neighbor[i]),
      .KILLING          (killing_signals[i]),

      // ---------------------------------------------------
      // Input Items
      // ---------------------------------------------------
      .LEFT_ID          (items_id[i+1]),
      .LEFT_DATA        (items_data[i+1]),
      .INPUT_ITEM       (INPUT_ITEM),
      .RIGHT_ID         (items_id[i-1]),
      .RIGHT_DATA       (items_data[i-1]),
      .KILL_ID          (KILL_ID),

      // ---------------------------------------------------
      // Output Item
      // ---------------------------------------------------
      .ITEM_ID          (items_id[i]),
      .ITEM_DATA        (items_data[i])
    );
  end
endgenerate

assign BEST_ITEM.id   = items_id[0];
assign BEST_ITEM.data = items_data[0];

/////////////////////////////////
//  For testing purposes only
// synthesis translate_off
memory_model_error_detector MMED (
  .clk            (CLK),
  .rst            (RST),
  .instr          (INSTR),
  .items_id       (items_id),
  .items_data     (items_data),
  .error          ()
);
// synthesis translate_on

endmodule : shift_regs_queue