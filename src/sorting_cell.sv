// Created by Lukas Kohutka

import shift_regs_queue_pkg::*;

//  ----------------------------------------------------------------------------
//
//  ENTITY DECLARATION ---------------------------------------------------------
//
// -----------------------------------------------------------------------------

module sorting_cell #(
  parameter i
)(
  // ---------------------------------------------------
  // Clock & Reset
  // ---------------------------------------------------
  input   logic   CLK,
  input   logic   RST,

  // ---------------------------------------------------
  // Input Control Signals
  // ---------------------------------------------------
  input   opcode  INSTR,       // from bus
  input   logic   SHIFT,
  input   logic   L_BETTER,
  input   logic   R_BETTER,

  // ---------------------------------------------------
  // Output Control Signals
  // ---------------------------------------------------
  output  logic   BETTER,        // goes to R_BETTER input to left cell
  output  logic   KILLING,

  // ---------------------------------------------------
  // Input Items
  // ---------------------------------------------------
  input   logic [ID_W-1:0]   LEFT_ID,        // item from LEFT
  input   logic [DATA_W-1:0] LEFT_DATA,      // item from LEFT
  input   s_item             INPUT_ITEM,     // item from bus
  input   logic [ID_W-1:0]   RIGHT_ID,       // item from RIGHT
  input   logic [DATA_W-1:0] RIGHT_DATA,     // item from RIGHT
  input   logic [ID_W-1:0]   KILL_ID,

  // ---------------------------------------------------
  // Output Item
  // ---------------------------------------------------
  output  logic [ID_W-1:0]   ITEM_ID,
  output  logic [DATA_W-1:0] ITEM_DATA
);

//  ----------------------------------------------------------------------------
//
// ARCHITECTURE DECLARATION  ---------------------------------------------------
//
// -----------------------------------------------------------------------------

s_item this_item;
s_item to_be_this_item;
logic update_this_item;

logic this_item_is_input;
logic kill_this_item;
logic read_left;

logic input_is_better;

// ----------------------------------------------------------------------------
//
// ARCHITECTURE BODY ----------------------------------------------------------
//
// ----------------------------------------------------------------------------

assign input_is_better = (MIN_MAX == MIN_QUEUE) ? (INPUT_ITEM.data < this_item.data) : (INPUT_ITEM.data > this_item.data);

// combinational logic responsible for deciding 
// whether to kill this task
assign this_item_is_input  = KILL_ID == this_item.id;
assign kill_this_item      = this_item_is_input & INSTR[1];
assign read_left           = kill_this_item | SHIFT;

generate  
  // combinational logic responsible for
  // implementation of update_this_item
  if (i > 0) begin
    assign update_this_item = INSTR[0] ? (INSTR[1] ? (input_is_better & ~SHIFT) | (read_left & ~input_is_better) : input_is_better) : INSTR[1] & read_left;
  end else begin
    assign update_this_item = (INSTR[1] & read_left) | (input_is_better & INSTR[0]);
  end
endgenerate

// combinational logic responsible for
// implementation of MUX trees
always_comb begin : select_task_comb
  if (INSTR[0] & R_BETTER) begin
    to_be_this_item.id   = RIGHT_ID;
    to_be_this_item.data = RIGHT_DATA;
  end else begin
    if (INSTR[0] & L_BETTER) begin
      to_be_this_item = INPUT_ITEM;
    end else begin
      to_be_this_item.id   = LEFT_ID;
      to_be_this_item.data = LEFT_DATA;
    end
  end
end : select_task_comb

// store task in the registers
always_ff @(posedge CLK) begin
  if (RST) begin
    this_item <= CONST_ITEM_RESET;
  end else begin
    if (update_this_item) begin
      this_item <= to_be_this_item;
    end
  end
end

assign ITEM_ID   = this_item.id;
assign ITEM_DATA = this_item.data;
assign KILLING   = kill_this_item;
assign BETTER    = input_is_better;

endmodule : sorting_cell