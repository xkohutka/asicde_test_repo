// Created by Lukas Kohutka

package shift_regs_queue_pkg;

// ---------------------------------------------------------------------------
// PARAMETERS
// ---------------------------------------------------------------------------

parameter CAPACITY = 64;
parameter ID_W     = 12;
parameter DATA_W   = 32;
parameter CAN_KILL = 1; // 1 = ON, 0 = OFF

typedef enum bit {MIN_QUEUE, MAX_QUEUE} min_max_t;
parameter min_max_t MIN_MAX = MIN_QUEUE;

// ---------------------------------------------------------------------------
// TEST PROCEDURE PARAMETERS
// ---------------------------------------------------------------------------

parameter TEST_ITERATIONS = 1000;
parameter DEBUG_ON        = 0;     // 1 = ON, 0 = OFF
parameter CLK_HALF_PERIOD = 10ns;  // CLK_HALF_PERIOD = clk period / 2;

// ---------------------------------------------------------------------------
// TYPES
// ---------------------------------------------------------------------------

parameter OPCODE_W = 2;
typedef logic [OPCODE_W-1:0] opcode;
typedef enum opcode {NOP = 2'b00, INSERT = 2'b01, POP = 2'b10, INPOP = 2'b11} opcode_t;

typedef struct {
  logic  [ID_W-1:0]    id;        // identification number of the task
  logic  [DATA_W-1:0]  data;      // priority of the task
} s_item;

parameter s_item CONST_ITEM_RESET_MIN = '{data: {DATA_W{1'b1}}, id: {ID_W{1'b0}}};
parameter s_item CONST_ITEM_RESET_MAX = '{data: {DATA_W{1'b0}}, id: {ID_W{1'b0}}};
parameter s_item CONST_ITEM_RESET = MIN_MAX == MAX_QUEUE ? CONST_ITEM_RESET_MAX : CONST_ITEM_RESET_MIN;

endpackage : shift_regs_queue_pkg