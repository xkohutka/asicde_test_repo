// Created by Lukas Kohutka

import shift_regs_queue_pkg::*;

//  ----------------------------------------------------------------------------
//
//  ENTITY DECLARATION ---------------------------------------------------------
//
// -----------------------------------------------------------------------------

module shift_regs_queue_tb #(
  parameter ITEM_CELLS_COUNT     = CAPACITY,
  parameter GUI_SIMULATION       = 1,               // 1 = GUI, 0 = BATCH
  parameter PROGRESS_GRANULARITY = 5                // how often is progress updated
)(
  output logic  clk,
  output logic  rst,
  
  output logic [ID_W-1:0] exp_items_count,
  output logic [ID_W-1:0] kill_id,
  
  output opcode_t  instr,
  
  output s_item item_in,
  output s_item item_out,
  output s_item exp_item_out
);

//  ----------------------------------------------------------------------------
//
// ARCHITECTURE DECLARATION  ---------------------------------------------------
//
// -----------------------------------------------------------------------------

logic [DATA_W+ID_W-1:0] queue[$];

// ---------------------------------------------------
// Inner Signals in Monitor & Scoreboard
// ---------------------------------------------------
int passes;
int fails;
bit predictor_output;

// ----------------------------------------------------------------------------
//
// ARCHITECTURE BODY ----------------------------------------------------------
//
// ----------------------------------------------------------------------------

// ---------------------------------------------------
// DUT instantiation
// ---------------------------------------------------
shift_regs_queue dut (
  // ---------------------------------------------------
  // Clock & Reset
  // ---------------------------------------------------
  .CLK              (clk),
  .RST              (rst),

  // ---------------------------------------------------
  // Input Control Signals
  // ---------------------------------------------------
  .INSTR            (instr),

  // ---------------------------------------------------
  // Input Data Signals
  // ---------------------------------------------------
  .INPUT_ITEM       (item_in),
  .KILL_ID          (kill_id),

  // ---------------------------------------------------
  // Output Data Signals
  // ---------------------------------------------------
  .BEST_ITEM        (item_out)
);

// ---------------------------------------------------
// Clock Implementation
// 20 ns period (50 MHz)
// ---------------------------------------------------
always #CLK_HALF_PERIOD clk = ~clk;

// ---------------------------------------------------
// Monitor & Scoreboard
// compares DUT output with expected output
// counts number of passes and fails
// ---------------------------------------------------
initial begin
  passes = 0;
  fails = 0;
end

always @(posedge clk) begin : monitor_and_scoreboard
  if (item_out.id === exp_item_out.id) begin
    if (DEBUG_ON)
      $display("ID PASSED:  %0d", item_out.id);
    passes++;
  end else begin
    $display("ID FAILED:  Expected: %0d   Got: %0d",
      exp_item_out.id, item_out.id);
    fails++;
  end

  if (item_out.data === exp_item_out.data) begin
    if (DEBUG_ON)
      $display("DATA PASSED: %0d", item_out.data);
    passes++;
  end else begin
    $display("DATA FAILED:  Expected: %0d   Got: %0d",
      exp_item_out.data, item_out.data);
    fails++;
  end
  
/*   if (items_count === queue.size()) begin
    if (DEBUG_ON)
      $display("ITEMS COUNT PASSED: %0d", items_count);
    passes++;
  end else begin
    $display("ITEMS COUNT FAILED:  Expected: %0d   Got: %0d",
      queue.size(), items_count);
    fails++;
  end */
end : monitor_and_scoreboard

// ---------------------------------------------------
// Predictor
// ---------------------------------------------------
always @(posedge clk) begin : predictor
  int i, queue_size;
  bit found;
  logic [DATA_W+ID_W-1:0] removed_item;
  
  if (rst) begin
    exp_item_out = CONST_ITEM_RESET;
    queue = {};
  end else begin
    // remove item from the queue
    if (instr[1]) begin
      if (CAN_KILL) begin
        while(1) begin
          found = 0;
          // search for a item with given ID
          for (i = 0; i < queue.size(); i++) begin
            if (queue[i][ID_W-1:0] == kill_id) begin
              found = 1;
              break;
            end
          end
          // kill found item
          if (found) begin
            queue.delete(i);
          // stop searching when nothing was found
          end else begin
            break;
          end
        end
      end else begin
        removed_item = queue.pop_front();
      end
    end
  
    // add item to the queue
    if (instr[0]) begin
      queue.push_back({item_in.data, item_in.id});
    end

    queue_size = queue.size();
    exp_items_count = queue_size;

    if (queue_size == 0) begin
      exp_item_out = CONST_ITEM_RESET;
    end else begin
      // sort the tasks so that
      // queue[0] is the task with the smallest deadline
      // and queue[1] is the task with the second smallest deadline
      queue.sort();
      if (DEBUG_ON) begin
        for (i = 0; i < queue_size; i++) begin
          $display("queue[%0d]: %0d", i, queue[i][DATA_W+ID_W-1:ID_W]);
        end
      end
      
      exp_item_out.id   = queue[0][ID_W-1:0];
      exp_item_out.data = queue[0][DATA_W+ID_W-1:ID_W];

    end
  end
end : predictor

// ---------------------------------------------------
// Test Procedure
// ---------------------------------------------------
initial begin : test_procedure
  int ti;
  logic [ID_W-1:0] uid;

  // initial values
  clk           = 0;
  rst           = 1;
  instr         = NOP;
  item_in.id    = 0;
  item_in.data  = 10;
  
  uid     = '0;
  kill_id = '0;
  
  if (DEBUG_ON)
    $display("RESET ASSERTED");
  rst = 1;

  @(negedge clk);

  if (DEBUG_ON)
    $display("RESET DEASSERTED");
  rst = 0;
    
  @(negedge clk);

  // wait one clock cycle
  @(negedge clk);

  for (ti = 1; ti <= TEST_ITERATIONS; ti++) begin
    if (ti % (TEST_ITERATIONS / (100 / PROGRESS_GRANULARITY)) == 0)
      $display("DONE: %0d%%", ti / (TEST_ITERATIONS / 100));
    
    uid = '0;
    kill_id = '0;

    if (DEBUG_ON)
      $display("ADDING ITEM");

    repeat(ITEM_CELLS_COUNT) begin
      instr = INSERT;
      uid += 1;
      item_in.id = uid;
      item_in.data = $urandom_range(2 ** DATA_W - 1, DATA_W);

      if (DEBUG_ON) begin
        $display("ITEM INSERT:  id: %0d   deadline: %0d",
                 item_in.id, item_in.data);
      end
      
      // wait four clock cycles
      @(negedge clk);
      instr = NOP;
      @(negedge clk);@(negedge clk);@(negedge clk);
    end

    if (DEBUG_ON)
      $display("ADDING AND KILLING ITEM");

    repeat(ITEM_CELLS_COUNT) begin
      instr = INPOP;
      uid += 1;
      kill_id += 1;
      item_in.id = uid;
      item_in.data = $urandom_range(2 ** DATA_W - 1, DATA_W);

      if (DEBUG_ON) begin
        $display("ITEM INSERT:  id: %0d   deadline: %0d",
                 item_in.id, item_in.data);
        $display("ITEM KILL:  id: %0d", kill_id);
      end
      
      // wait four clock cycles
      @(negedge clk);
      instr = NOP;
      @(negedge clk);@(negedge clk);@(negedge clk);
    end

    if (DEBUG_ON)
      $display("KILLING ITEM");
    
    repeat(ITEM_CELLS_COUNT) begin
      instr = POP;
      kill_id += 1;

      if (DEBUG_ON) begin
        $display("ITEM KILL:  id: %0d", kill_id);
      end
      
      // wait four clock cycles
      @(negedge clk);
      instr = NOP;
      @(negedge clk);@(negedge clk);@(negedge clk);
    end
    
  end;

  $display("-----------------------------------------------------");
  $display("----------------  END OF SIMULATION  ----------------");
  $display("-----------------------------------------------------");
  
  $display("Passed: %0d", passes);
  $display("Failed: %0d", fails);
  $display("Success: %2f%%", real'(100 * passes) /  real'(passes + fails));

  // end of simulation
  if (GUI_SIMULATION)
    $stop; // use for gui simulation
  $finish; // use for batch simulation
end : test_procedure

endmodule : shift_regs_queue_tb