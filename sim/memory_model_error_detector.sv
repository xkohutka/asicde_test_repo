// Created by Lukas Kohutka

import shift_regs_queue_pkg::*;

module memory_model_error_detector (
  input logic                           clk,
  input logic                           rst,
  input opcode                          instr,
  input logic [CAPACITY:-1][ID_W-1:0]   items_id,
  input logic [CAPACITY:-1][DATA_W-1:0] items_data,
  output logic                          error
);  
  // Errors
  logic [CAPACITY-1:0] order_error     = '0;
  logic [CAPACITY-1:0] unique_error    = '0;
  
  integer items_cnt = 0;
  integer i, j, k;
  
  //////////////////////////////////////////////////////
  
  always_ff @(posedge clk) begin
    if (rst) begin
      items_cnt <= 0;
    end else begin
      if (instr == INSERT && items_cnt < CAPACITY) begin
        items_cnt <= items_cnt + 1;
      end
      if (instr == POP && items_cnt) begin
        items_cnt <= items_cnt - 1;
      end
    end
  end
  
  assign error = (|order_error) || (|unique_error);
   
  // Data must remain ordered
  always_comb begin
    order_error = '0;
//    if (ready) begin
      for (i = 1; i < items_cnt; i = i + 1) begin
        order_error[i] = MIN_MAX == MAX_QUEUE ? items_data[i] > items_data[i-1] : items_data[i] < items_data[i-1];
        if (order_error[i]) begin
          $display("DATA NOT ORDERED: address %0d and %0d", i, i-1);
        end
      end
//    end
  end
  
  // Data must be unique
  always_comb begin
    unique_error = '0;
//    if (ready) begin
      for (j = 0; j < items_cnt; j = j + 1) begin
        unique_error[j] = 1'b0;
        for (k = j + 1; k < items_cnt; k = k + 1) begin
          if (j != k) begin
            if (items_id[j] == items_id[k]) begin
              unique_error[j] = 1'b1;
              unique_error[k] = 1'b1;
              $display("ITEMS NOT UNIQUE: address %0d and %0d", j, k);
            end
          end
        end
      end
//    end
  end

endmodule